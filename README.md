- [Unsupervised Machine Learning](#unsupervised-machine-learning)
  - [Clustering](#clustering)
- [Data preparation](#data-preparation)
  - [Transformation Example](#transformation-example)
  - [Data Transformation Example 2](#data-transformation-example-2)
- [K-Means Algorithm](#k-means-algorithm)
  - [The Elbow Curve](#the-elbow-curve)
- [K-Cluster Example](#k-cluster-example)
  - [Load the Data](#load-the-data)
  - [Convert time to Time Delta](#convert-time-to-time-delta)
  - [Clean the data](#clean-the-data)
  - [Creating a Training Set](#creating-a-training-set)
  - [Chose Number of Clusters and Predict](#chose-number-of-clusters-and-predict)
  - [Visualize the Results](#visualize-the-results)
  - [Organize the Results](#organize-the-results)
- [Principal Component Analysis](#principal-component-analysis)
- [Example of PCA](#example-of-pca)
  - [Load the Data](#load-the-data)
  - [Standarize / Scale the data](#standarize-scale-the-data)
  - [Use the PCA](#use-the-pca)
  - [K-means clustering](#k-means-clustering)
  - [Visualize the Results](#visualize-the-results)
- [PCA Example 2](#pca-example-2)
  - [Clean the data](#clean-the-data)
  - [Applying PCA](#applying-pca)
  - [Visualize PCA](#visualize-pca)
  - [Visualize Elbow Curve](#visualize-elbow-curve)
  - [Select Amount of Clusters and build Model](#select-amount-of-clusters-and-build-model)
  - [Visualize the Results](#visualize-the-results)
  - [Display summary statistics](#display-summary-statistics)
- [Hierarchical Clustering](#hierarchical-clustering)
  - [Dendograms](#dendograms)
- [Hierarchical Cluster Example](#hierarchical-cluster-example)
  - [Imports](#imports)
  - [Load and Normalize the Data](#load-and-normalize-the-data)
  - [Create the Dendogram](#create-the-dendogram)
  - [Remap Observations into Clusters](#remap-observations-into-clusters)
  - [Run the Dendogram Again](#run-the-dendogram-again)




<a id="unsupervised-machine-learning"></a>

# Unsupervised Machine Learning

Unsupervised ML allows us to find hidden patterns in our data to get observations to help us make decisions or change our perspective on the data.

The main difference against Supervised Learning is that there is no target variable. We will build labels from the patterns that arise by clustering information, so we will focus on the **features** and we will work on them.

Unsupervised learning requires more human work and as it is an iteractive process.

-   Some use cases are grouping unlabeled data into distinct clusters.

-   Detecting unusual data points in a dataset (anomaly detection).

-   Reducing large dataset into smaller datasets while preserving most of the useful information (dimensionality reduction.)


<a id="clustering"></a>

## Clustering

We can use clustering to group patterns into different clusters.

Anomaly detection is when we use clustering to identify datapoints that may be suspicious (like credit card fraudulent transactions).

When doing dimensionality reduction, we reduce N number of features into a handful of features or a single one. This allows us to generate labels that we can use to feed to the Supervised Learning models.

A very popular example is customer segmentation, which allows companies to detect habits and references and build data on top of that to create predictions. For example, personalized movies suggestions in Netflix, Amazon, etc.


<a id="data-preparation"></a>

# Data preparation

Data preparation is the process of converting the data of whichever nature to numbers, for example dates to time measurements or labels into integers.

We will convert the text data from the dataset to integer labels.

```python
import pandas as pd
from pathlib import Path


file_path = Path('../resources/iris.csv')
df = pd.read_csv(file_path)
print(df.head(5))
```

       sepal_length  sepal_width  petal_length  petal_width        class
    0           5.1          3.5           1.4          0.2  Iris-setosa
    1           4.9          3.0           1.4          0.2  Iris-setosa
    2           4.7          3.2           1.3          0.2  Iris-setosa
    3           4.6          3.1           1.5          0.2  Iris-setosa
    4           5.0          3.6           1.4          0.2  Iris-setosa

```python
class_dict = {'Iris-setosa': 0, 'Iris-versicolor': 1, 'Iris-virginica': 2}
df2 = df.replace({'class': class_dict})
print(df2.head())
```

       sepal_length  sepal_width  ...  petal_width  class
    0           5.1          3.5  ...          0.2      0
    1           4.9          3.0  ...          0.2      0
    2           4.7          3.2  ...          0.2      0
    3           4.6          3.1  ...          0.2      0
    4           5.0          3.6  ...          0.2      0
    
    [5 rows x 5 columns]

```python
new_iris_df = df.drop(["class"], axis=1)
print(new_iris_df.head(5))
```

       sepal_length  sepal_width  petal_length  petal_width
    0           5.1          3.5           1.4          0.2
    1           4.9          3.0           1.4          0.2
    2           4.7          3.2           1.3          0.2
    3           4.6          3.1           1.5          0.2
    4           5.0          3.6           1.4          0.2


<a id="transformation-example"></a>

## Transformation Example

You are given a dataset that contains historical data from purchases of an online store made by 200 customers. In this activity you will put in action your data preprocessing superpowers, also you&rsquo;ll add some new skills needed to start finding customers clusters.

```python
import pandas as pd
from pathlib import Path

file_path = Path('../resources/shopping_data.csv')
df_shopping = pd.read_csv(file_path)
print(df_shopping.dtypes)
print(df_shopping['Previous Shopper'].head())
print(df_shopping['Age'].head())
print(df_shopping['Annual Income'].head())
print(df_shopping['Spending Score (1-100)'].head())
```

    CustomerID                 int64
    Previous Shopper          object
    Age                        int64
    Annual Income              int64
    Spending Score (1-100)     int64
    dtype: object
    0    Yes
    1    Yes
    2     No
    3     No
    4     No
    Name: Previous Shopper, dtype: object
    0    19
    1    21
    2    20
    3    23
    4    31
    Name: Age, dtype: int64
    0    15000
    1    15000
    2    16000
    3    16000
    4    17000
    Name: Annual Income, dtype: int64
    0    39
    1    81
    2     6
    3    77
    4    40
    Name: Spending Score (1-100), dtype: int64

We will drop the data that we don&rsquo;t want.

```python
df_shopping = df_shopping.drop(['CustomerID'], axis=1)
print(df_shopping.head())
```

      Previous Shopper  ...  Spending Score (1-100)
    0              Yes  ...                      39
    1              Yes  ...                      81
    2               No  ...                       6
    3               No  ...                      77
    4               No  ...                      40
    
    [5 rows x 4 columns]

We will drop the null values.

```python
print(df_shopping.shape)
df_shopping.dropna(inplace=True)
print(df_shopping.shape)
```

    (200, 4)
    (200, 4)

Now we replace the non-numeric data.

```python
def convert_status(status):
    return 1 if status == 'Yes' else 0
df_shopping["Previous Shopper"] = df_shopping["Previous Shopper"].map(convert_status)
print(df_shopping["Previous Shopper"].head())
```

    0    1
    1    1
    2    0
    3    0
    4    0
    Name: Previous Shopper, dtype: int64

We want to normalize the data, scale it.

```python
df_shopping["Annual Income"] = df_shopping["Annual Income"] / 1000
print(df_shopping.head())
file_path = Path('../resources/shopping_cleaned.csv')
pd.to_csv(file_path, index=False)
```

       Previous Shopper  ...  Spending Score (1-100)
    0                 1  ...                      39
    1                 1  ...                      81
    2                 0  ...                       6
    3                 0  ...                      77
    4                 0  ...                      40
    
    [5 rows x 4 columns]


<a id="data-transformation-example-2"></a>

## Data Transformation Example 2

```python
import pandas as pd
from sklearn.preprocessing import MinMaxScaler, LabelEncoder

df = pd.read_csv('../resources/marathon_2019.csv')
print(df.head())
```

      Bib               Name  Age M/F      City State  ...     Pace Proj Time Official Time Overall Gender Division
    0   2  Cherono, Lawrence   30   M   Eldoret   NaN  ...  0:04:53       NaN       2:07:57       1      1        1
    1   6     Desisa, Lelisa   29   M      Ambo   NaN  ...  0:04:53       NaN       2:07:59       2      2        2
    2   7  Kipkemoi, Kenneth   34   M   Eldoret   NaN  ...  0:04:54       NaN       2:08:07       3      3        3
    3   8      Kandie, Felix   32   M      Iten   NaN  ...  0:04:55       NaN       2:08:54       4      4        4
    4  11    Kirui, Geoffrey   26   M  Keringet   NaN  ...  0:04:56       NaN       2:08:55       5      5        5
    
    [5 rows x 24 columns]

Subset the dataframe to only the columns &ldquo;Age&rdquo;, &ldquo;M/F&rdquo;, split times (i.e. &ldquo;5K, 10K&rdquo;, etc.), &ldquo;Pace&rdquo;, and &ldquo;Official Time&rdquo;.

```python
df = df[["Age", "M/F", "5K", "10K", "15K", "20K", "Pace", "Official Time"]]
print(df.head())
```

       Age M/F       5K      10K      15K      20K     Pace Official Time
    0   30   M  0:15:11  0:30:21  0:45:48  1:01:16  0:04:53       2:07:57
    1   29   M  0:15:10  0:30:22  0:45:46  1:01:16  0:04:53       2:07:59
    2   34   M  0:15:14  0:30:22  0:45:47  1:01:17  0:04:54       2:08:07
    3   32   M  0:15:14  0:30:24  0:45:47  1:01:16  0:04:55       2:08:54
    4   26   M  0:15:12  0:30:21  0:45:46  1:01:15  0:04:56       2:08:55

```python
print(df.shape)
df.dropna(inplace=True)
print(df.shape)
```

    (26647, 8)
    (26647, 8)

Convert the time data. We we are going to convert the data to time delta to get how long it took each athlete reach the goal.

```python
time_columns = ["5K", "10K", "15K", "20K", "Pace", "Official Time"]
df[time_columns] = df[time_columns].apply(lambda x: x.replace('-', '0')).apply(pd.to_timedelta)
print(df.head())
```

       Age M/F              5K             10K             15K             20K            Pace   Official Time
    0   30   M 0 days 00:15:11 0 days 00:30:21 0 days 00:45:48 0 days 01:01:16 0 days 00:04:53 0 days 02:07:57
    1   29   M 0 days 00:15:10 0 days 00:30:22 0 days 00:45:46 0 days 01:01:16 0 days 00:04:53 0 days 02:07:59
    2   34   M 0 days 00:15:14 0 days 00:30:22 0 days 00:45:47 0 days 01:01:17 0 days 00:04:54 0 days 02:08:07
    3   32   M 0 days 00:15:14 0 days 00:30:24 0 days 00:45:47 0 days 01:01:16 0 days 00:04:55 0 days 02:08:54
    4   26   M 0 days 00:15:12 0 days 00:30:21 0 days 00:45:46 0 days 01:01:15 0 days 00:04:56 0 days 02:08:55

Now we convert time units to seconds so we can process them as integer units.

```python
df[time_columns] = df[time_columns].apply(lambda x: x.dt.total_seconds())
print(df.head())
```

       Age M/F     5K     10K     15K     20K   Pace  Official Time
    0   30   M  911.0  1821.0  2748.0  3676.0  293.0         7677.0
    1   29   M  910.0  1822.0  2746.0  3676.0  293.0         7679.0
    2   34   M  914.0  1822.0  2747.0  3677.0  294.0         7687.0
    3   32   M  914.0  1824.0  2747.0  3676.0  295.0         7734.0
    4   26   M  912.0  1821.0  2746.0  3675.0  296.0         7735.0

We check for zeroes.

```python
print(df.shape)
df = df[~(df == 0).any(axis=1)]
print(df.shape)
```

    (26647, 8)
    (24529, 8)

Converting labels to numbers using LabelEncoder. This is a scikit learn to convert labels to integers. The fit transform function is fit and predict for the model on some given data. It returns either a 1 or a 2.

```python
df['M/F'] = LabelEncoder().fit_transform(df['M/F'])
print(df.head())
```

       Age  M/F     5K     10K     15K     20K   Pace  Official Time
    0   30    1  911.0  1821.0  2748.0  3676.0  293.0         7677.0
    1   29    1  910.0  1822.0  2746.0  3676.0  293.0         7679.0
    2   34    1  914.0  1822.0  2747.0  3677.0  294.0         7687.0
    3   32    1  914.0  1824.0  2747.0  3676.0  295.0         7734.0
    4   26    1  912.0  1821.0  2746.0  3675.0  296.0         7735.0

We check for types.

```python
print(df.dtypes)
```

    Age                int64
    M/F                int64
    5K               float64
    10K              float64
    15K              float64
    20K              float64
    Pace             float64
    Official Time    float64
    dtype: object

```python
df['Age'] = pd.to_numeric(df['Age'])
print(df['Age'].head())
```

    0    30
    1    29
    2    34
    3    32
    4    26
    Name: Age, dtype: int64

Check for correlation.

```python
from matplotlib import pyplot as plt

file = '../resources/correlation.png'
plot = df.plot(kind='scatter', x='Pace', y='Official Time')
plt.savefig(file)
print(file)
```

<div class="org" id="org1828b92">

<div id="org7bb8f3f" class="figure">
<p><img src="../resources/correlation.png" alt="correlation.png" />
</p>
</div>

</div>

Finally we scale the data.

```python
X = df.drop("Pace", axis=1)
X_scaled = MinMaxScaler().fit_transform(X)
print(X_scaled[:5])
```

    [[1.84615385e-01 1.00000000e+00 2.72108844e-02 0.00000000e+00
      4.94722955e-04 2.45730434e-04 0.00000000e+00]
     [1.69230769e-01 1.00000000e+00 2.68107243e-02 2.49066002e-04
      1.64907652e-04 2.45730434e-04 1.04920785e-04]
     [2.46153846e-01 1.00000000e+00 2.84113645e-02 2.49066002e-04
      3.29815303e-04 3.68595651e-04 5.24603924e-04]
     [2.15384615e-01 1.00000000e+00 2.84113645e-02 7.47198007e-04
      3.29815303e-04 2.45730434e-04 2.99024237e-03]
     [1.23076923e-01 1.00000000e+00 2.76110444e-02 0.00000000e+00
      1.64907652e-04 1.22865217e-04 3.04270276e-03]]

Now we can compare the scaled data to the original one.

```python
print(X[:5])
```

       Age  M/F     5K     10K     15K     20K  Official Time
    0   30    1  911.0  1821.0  2748.0  3676.0         7677.0
    1   29    1  910.0  1822.0  2746.0  3676.0         7679.0
    2   34    1  914.0  1822.0  2747.0  3677.0         7687.0
    3   32    1  914.0  1824.0  2747.0  3676.0         7734.0
    4   26    1  912.0  1821.0  2746.0  3675.0         7735.0


<a id="k-means-algorithm"></a>

# K-Means Algorithm

K-Means groups together observations that have similar features.

K-means works by finding centroids in the plane and measure the distance from the centroid to the observation. It will optimize the centroid position that maximazes the distance to other centroid positions and minimizes the distance between all possible observations that can be closest to the centroid.

We consider an observation part of the cluster if it is within the radius of the cluster.

The **k** referes to the number of clusters we want to computer.

1.  Randomly initialize the k starting centroids.
2.  Each data point is assigned to its nearest centroid.
3.  The centroids are recomputed as the mean of the data points assigned to respective clsuter.
4.  Repeat 1 to 3 until the stopping criteria is triggered.


<a id="the-elbow-curve"></a>

## The Elbow Curve

The best number of **K** can be determined using the elbow curve. The x-axis is the **k** value and the y-axis is some objective function.

We will start asking a high number of clusters and we will measure the inertia, then ask for a lower number and continue measuring the inertia. Whenever the inertia goes from a high slope to a lower slope in the curve (as an elbow), we use that point as our **k** value.


<a id="k-cluster-example"></a>

# K-Cluster Example


<a id="load-the-data"></a>

## Load the Data

```python
import pandas as pd
from sklearn.preprocessing import MinMaxScaler, LabelEncoder

# Import the KMeans module and matplotlib
from sklearn.cluster import KMeans
from matplotlib import pyplot as plt

df = pd.read_csv('../resources/marathon_2019.csv')
df = df[['Age', 'M/F', '5K', '10K', '15K', '20K', 'Half', '25K', '30K', '35K', '40K', 'Pace', 'Official Time']]
print(df.head())
```

       Age M/F       5K      10K      15K      20K     Half      25K      30K      35K      40K     Pace Official Time
    0   30   M  0:15:11  0:30:21  0:45:48  1:01:16  1:04:29  1:16:23  1:32:01  1:47:16  2:01:45  0:04:53       2:07:57
    1   29   M  0:15:10  0:30:22  0:45:46  1:01:16  1:04:30  1:16:24  1:32:01  1:47:16  2:01:46  0:04:53       2:07:59
    2   34   M  0:15:14  0:30:22  0:45:47  1:01:17  1:04:31  1:16:24  1:32:01  1:47:16  2:01:45  0:04:54       2:08:07
    3   32   M  0:15:14  0:30:24  0:45:47  1:01:16  1:04:31  1:16:24  1:32:01  1:47:16  2:02:08  0:04:55       2:08:54
    4   26   M  0:15:12  0:30:21  0:45:46  1:01:15  1:04:28  1:16:23  1:32:01  1:47:16  2:01:57  0:04:56       2:08:55


<a id="convert-time-to-time-delta"></a>

## Convert time to Time Delta

We want to convert the time units into seconds. So we convert it to timedelta first, then to total seconds.

```python
time_columns = ["5K", "10K", "15K", "20K", "Half", "25K", "30K", "35K", "40K", "Pace", "Official Time"]
df[time_columns] = df[time_columns].apply(lambda x: x.replace('-', '0')).apply(pd.to_timedelta)
df[time_columns] = df[time_columns].apply(lambda x: x.dt.total_seconds())
print(df.head())
```

       Age M/F     5K     10K     15K     20K    Half     25K     30K     35K     40K   Pace  Official Time
    0   30   M  911.0  1821.0  2748.0  3676.0  3869.0  4583.0  5521.0  6436.0  7305.0  293.0         7677.0
    1   29   M  910.0  1822.0  2746.0  3676.0  3870.0  4584.0  5521.0  6436.0  7306.0  293.0         7679.0
    2   34   M  914.0  1822.0  2747.0  3677.0  3871.0  4584.0  5521.0  6436.0  7305.0  294.0         7687.0
    3   32   M  914.0  1824.0  2747.0  3676.0  3871.0  4584.0  5521.0  6436.0  7328.0  295.0         7734.0
    4   26   M  912.0  1821.0  2746.0  3675.0  3868.0  4583.0  5521.0  6436.0  7317.0  296.0         7735.0


<a id="clean-the-data"></a>

## Clean the data

We will subset the dataframe again, removing all zeroes. Then encode the labels into integers.

```python
df[~(df == 0).any(axis=1)]
df['M/F'] = LabelEncoder().fit_transform(df['M/F'])
print(df['M/F'].head())
```

    0    1
    1    1
    2    1
    3    1
    4    1
    Name: M/F, dtype: int64

Convert to numerics values.

```python
df['Age'] = pd.to_numeric(df['Age'])
print(df['Age'].head())
```

    0    30
    1    29
    2    34
    3    32
    4    26
    Name: Age, dtype: int64

Check correlation of data. It should be linear.

```python
imgpath = "../resources/correlation2.png"
fig, ax = plt.subplots(figsize=(5, 3))
ax.scatter(x=df['Pace'], y=df['Official Time'])
fig.savefig(imgpath)
print(imgpath)
```

<div class="org" id="org4484bd6">

<div id="orgec0dabc" class="figure">
<p><img src="../resources/correlation2.png" alt="correlation2.png" />
</p>
</div>

</div>


<a id="creating-a-training-set"></a>

## Creating a Training Set

Create a training set &rsquo;X&rsquo; without the &rsquo;Pace&rsquo; column.

```python
X = df.drop('Pace', axis=1)
X_scaled = MinMaxScaler().fit_transform(X)
print(X_scaled[:5])
```

    [[1.84615385e-01 1.00000000e+00 2.72591263e-01 3.12028787e-01
      3.11953684e-01 2.96499435e-01 2.95682079e-01 2.83163423e-01
      2.75073489e-01 2.70272540e-01 2.75670780e-01 0.00000000e+00]
     [1.69230769e-01 1.00000000e+00 2.72292041e-01 3.12200137e-01
      3.11726643e-01 2.96499435e-01 2.95758502e-01 2.83225209e-01
      2.75073489e-01 2.70272540e-01 2.75708517e-01 9.92506575e-05]
     [2.46153846e-01 1.00000000e+00 2.73488929e-01 3.12200137e-01
      3.11840163e-01 2.96580094e-01 2.95834925e-01 2.83225209e-01
      2.75073489e-01 2.70272540e-01 2.75670780e-01 4.96253288e-04]
     [2.15384615e-01 1.00000000e+00 2.73488929e-01 3.12542838e-01
      3.11840163e-01 2.96499435e-01 2.95834925e-01 2.83225209e-01
      2.75073489e-01 2.70272540e-01 2.76538737e-01 2.82864374e-03]
     [1.23076923e-01 1.00000000e+00 2.72890485e-01 3.12028787e-01
      3.11726643e-01 2.96418777e-01 2.95605655e-01 2.83163423e-01
      2.75073489e-01 2.70272540e-01 2.76123627e-01 2.87826907e-03]]

Perform K-Means Clustering. We will test for many **k** values. Then plot the Elbow curve graph.

```python
file = "../resources/elbow1.png"
sse = {}
K = range(1,10)
for k in K:
    kmeanmodel = KMeans(n_clusters=k).fit(X_scaled)
    sse[k]= kmeanmodel.inertia_

# Plot
fig, ax = plt.subplots(figsize=(6, 4))
ax.plot(list(sse.keys()), list(sse.values()))
ax.set_xlabel('k')
ax.set_ylabel('SSE')
ax.set_title('Elbow Method')
fig.savefig(file)
print(file)
```

<div class="org" id="org064c385">

<div id="org69b9c76" class="figure">
<p><img src="../resources/elbow1.png" alt="elbow1.png" />
</p>
</div>

</div>


<a id="chose-number-of-clusters-and-predict"></a>

## Chose Number of Clusters and Predict

Create a KMeans model with 3 clusters. Then we calculate the predicted values and add them to the dataframe.

```python
model = KMeans(n_clusters=3, random_state=42).fit(X_scaled)

y_pred = model.predict(X_scaled)
df_y = pd.DataFrame(y_pred, columns=['Cluster'])
combined = df.join(df_y, how='inner')
print(combined.head())
```

       Age  M/F     5K     10K     15K     20K    Half     25K     30K     35K     40K   Pace  Official Time  Cluster
    0   30    1  911.0  1821.0  2748.0  3676.0  3869.0  4583.0  5521.0  6436.0  7305.0  293.0         7677.0        2
    1   29    1  910.0  1822.0  2746.0  3676.0  3870.0  4584.0  5521.0  6436.0  7306.0  293.0         7679.0        2
    2   34    1  914.0  1822.0  2747.0  3677.0  3871.0  4584.0  5521.0  6436.0  7305.0  294.0         7687.0        2
    3   32    1  914.0  1824.0  2747.0  3676.0  3871.0  4584.0  5521.0  6436.0  7328.0  295.0         7734.0        2
    4   26    1  912.0  1821.0  2746.0  3675.0  3868.0  4583.0  5521.0  6436.0  7317.0  296.0         7735.0        2


<a id="visualize-the-results"></a>

## Visualize the Results

Then we can create a boxplot to visualize the data for different clusters.

```python
file = "../resources/boxplot1.png"

fig, ax = plt.subplots(figsize=(6, 4))
ax.boxplot([combined["Official Time"][combined['Cluster'] == c] for c in combined['Cluster'].unique()])
ax.set_ylabel('Official Time')
ax.set_title('Elbow Method')
fig.savefig(file)
print(file)
```

<div class="org" id="org2821df8">

<div id="org62f05bc" class="figure">
<p><img src="../resources/boxplot1.png" alt="boxplot1.png" />
</p>
</div>

</div>

```python
print(combined.groupby(['M/F','Cluster']).describe()['Age'])
```

                   count       mean        std   min   25%   50%   75%   max
    M/F Cluster
    0   1        11981.0  40.545197  10.962787  18.0  31.0  40.0  48.0  80.0
    1   0         5144.0  51.623250  11.951811  18.0  44.0  54.0  60.0  83.0
        2         9522.0  40.872821   9.594296  18.0  34.0  41.0  48.0  71.0


<a id="organize-the-results"></a>

## Organize the Results

Then we re-assign the labels via a function. We create a function that takes in gender and age and assigns an age group based on the following break points for each gender:

-   The lowest 1st quartile
-   Each median
-   The highest 3rd quartile

Then we apply the custom age group to the original data frame and save it to the column &rsquo;Age Group&rsquo;.

```python
def age_group(gender, age):
    if gender == 0:
        if age < 29:
            return 0
        elif age < 36:
            return 1
        elif age < 41:
            return 2
        elif age < 45:
            return 3
        elif age < 51:
            return 4
        else:
            return 5
    if gender == 1:
        if age < 33:
            return 0
        elif age < 40:
            return 1
        elif age < 48:
            return 2
        elif age < 53:
            return 3
        elif age < 60:
            return 4
        else:
            return 5
df['Age Group'] = df.apply(lambda row: age_group(row['M/F'], row['Age']), axis=1)
print(df.head())
```

       Age  M/F     5K     10K     15K     20K    Half     25K     30K     35K     40K   Pace  Official Time  Age Group
    0   30    1  911.0  1821.0  2748.0  3676.0  3869.0  4583.0  5521.0  6436.0  7305.0  293.0         7677.0          0
    1   29    1  910.0  1822.0  2746.0  3676.0  3870.0  4584.0  5521.0  6436.0  7306.0  293.0         7679.0          0
    2   34    1  914.0  1822.0  2747.0  3677.0  3871.0  4584.0  5521.0  6436.0  7305.0  294.0         7687.0          1
    3   32    1  914.0  1824.0  2747.0  3676.0  3871.0  4584.0  5521.0  6436.0  7328.0  295.0         7734.0          0
    4   26    1  912.0  1821.0  2746.0  3675.0  3868.0  4583.0  5521.0  6436.0  7317.0  296.0         7735.0          0


<a id="principal-component-analysis"></a>

# Principal Component Analysis

PCA reduces the dataset into a smaller set of dimensions called **principal components**. The objective is to have a very well represented set of components for our feature set, even if it&rsquo;s hard for us to understand why this is.

The principal components don&rsquo;t look a lot like the original dataset. We will always lose data but we want to keep as much information as possible. If we lose too much data, we risk losing an important part of the variation we are looking at.

The **explained variance ratio** tells us how much of each component captures the variance of the dimensions.

This is an iterative process where we can go back to increase the number of components to see if we can capture more data or if we can work with the current number of components. We normally want to aim for over 90 percent of the variance ratio.


<a id="example-of-pca"></a>

# Example of PCA


<a id="load-the-data"></a>

## Load the Data

We want to reduce the dimensions of the consumers shopping dataset from 4 to 2 featues. Then we can use K-means for segmentation.

```python
import pandas as pd
from pathlib import Path
from sklearn.preprocessing import StandardScaler
from sklearn.decomposition import PCA
from sklearn.cluster import KMeans

file_path = Path("../resources/shopping_data_cleaned.csv")
df = pd.read_csv(file_path)
print(df.head())
```

       Previous Shopper  Age  Annual Income  Spending Score (1-100)
    0                 1   19           15.0                      39
    1                 1   21           15.0                      81
    2                 0   20           16.0                       6
    3                 0   23           16.0                      77
    4                 0   31           17.0                      40


<a id="standarize-scale-the-data"></a>

## Standarize / Scale the data

```python
df_scaled = StandardScaler().fit_transform(df)
print(df_scaled[0:5])
```

    [[ 1.12815215 -1.42456879 -1.73899919 -0.43480148]
     [ 1.12815215 -1.28103541 -1.73899919  1.19570407]
     [-0.88640526 -1.3528021  -1.70082976 -1.71591298]
     [-0.88640526 -1.13750203 -1.70082976  1.04041783]
     [-0.88640526 -0.56336851 -1.66266033 -0.39597992]]


<a id="use-the-pca"></a>

## Use the PCA

Then we transform the PCA data to a dataframe and we take a look at the explained variance ratio.

```python
pca = PCA(n_components=2)
data = pca.fit_transform(df_scaled)

df_pca = pd.DataFrame(
    data=data,
    columns=["principal component 1", "principal component 2"]
)
print(df_pca.head())
print(pca.explained_variance_ratio_)
```

       principal component 1  principal component 2
    0              -0.406383              -0.520714
    1              -1.427673              -0.367310
    2               0.050761              -1.894068
    3              -1.694513              -1.631908
    4              -0.313108              -1.810483
    [0.33690046 0.26230645]

We have a total of 60% of the information from the two components. We will see wether increasing the number of components will incrase it.

```python
pca = PCA(n_components=3)
data = pca.fit_transform(df_scaled)

df_pca = pd.DataFrame(
    data=data,
    columns=["PC1", "PC2", "PC3"]
)
print(df_pca.head())
print(pca.explained_variance_ratio_)
```

            PC1       PC2       PC3
    0 -0.406383 -0.520714 -2.072527
    1 -1.427673 -0.367310 -2.277644
    2  0.050761 -1.894068 -0.367375
    3 -1.694513 -1.631908 -0.717467
    4 -0.313108 -1.810483 -0.426460
    [0.33690046 0.26230645 0.23260639]

We get a total of 83 percent of the information. This is good enough to continue.


<a id="k-means-clustering"></a>

## K-means clustering

Now we can use a K-means clustering algorithm.

```python
model = KMeans(n_clusters=k, random_state=0).fit(df_pca)

predictions = model.predict(df_pca)
df_pca["class"] = model.labels_
print(df_pca.head())
```

            PC1       PC2       PC3  class
    0 -0.406383 -0.520714 -2.072527      4
    1 -1.427673 -0.367310 -2.277644      4
    2  0.050761 -1.894068 -0.367375      2
    3 -1.694513 -1.631908 -0.717467      8
    4 -0.313108 -1.810483 -0.426460      8


<a id="visualize-the-results"></a>

## Visualize the Results

We will get a 3D plane in this case with all the new features spread accross it.

```python
import plotly.express as px
fig = px.scatter_3d(
    df_pca,
    x="PC3",
    y="PC2",
    z="PC1",
    color="class",
    symbol="class",
    width=800,
)
fig.update_layout(legend=dict(x=0, y=1))
fig.show()
```

![img](../resources/pca1.png)

![img](../resources/pca2.png)


<a id="pca-example-2"></a>

# PCA Example 2

We will go back to the last K-means example and use that data to apply PCA to it.


<a id="clean-the-data"></a>

## Clean the data

We will get rid of the columns we are not interested in.

```python
X = df.drop(["Age", "Pace"], axis=1)
X_scaled = MinMaxScaler().fit_transform(X)
print(X_scaled[:5])
```

    [[1.00000000e+00 2.72591263e-01 3.12028787e-01 3.11953684e-01
      2.96499435e-01 2.95682079e-01 2.83163423e-01 2.75073489e-01
      2.70272540e-01 2.75670780e-01 0.00000000e+00 0.00000000e+00]
     [1.00000000e+00 2.72292041e-01 3.12200137e-01 3.11726643e-01
      2.96499435e-01 2.95758502e-01 2.83225209e-01 2.75073489e-01
      2.70272540e-01 2.75708517e-01 9.92506575e-05 0.00000000e+00]
     [1.00000000e+00 2.73488929e-01 3.12200137e-01 3.11840163e-01
      2.96580094e-01 2.95834925e-01 2.83225209e-01 2.75073489e-01
      2.70272540e-01 2.75670780e-01 4.96253288e-04 2.00000000e-01]
     [1.00000000e+00 2.73488929e-01 3.12542838e-01 3.11840163e-01
      2.96499435e-01 2.95834925e-01 2.83225209e-01 2.75073489e-01
      2.70272540e-01 2.76538737e-01 2.82864374e-03 0.00000000e+00]
     [1.00000000e+00 2.72890485e-01 3.12028787e-01 3.11726643e-01
      2.96418777e-01 2.95605655e-01 2.83163423e-01 2.75073489e-01
      2.70272540e-01 2.76123627e-01 2.87826907e-03 0.00000000e+00]]

We can observe that we currently have 12 features. So we are going to use PCA to simplify this.


<a id="applying-pca"></a>

## Applying PCA

We will use two dimensions to start with.

```python
pca = PCA(n_components=2)
pca.fit(X_scaled)
X_pca = pca.transform(X_scaled)
print(pca.explained_variance_ratio_)
```

    [0.55211069 0.26751402]

Note that the first dimension explains 55 percent of all twelve dimensions. This is because most of our features are highly correlated, in this case, most are related to speed and endurance of the runners so even if these dimensions don&rsquo;t relate specifically to those two labels, we can use them for creating our clusters.


<a id="visualize-pca"></a>

## Visualize PCA

```python
import hvplot.pandas
import hvplot

df_pca = pd.DataFrame(X_pca, columns=["PC1", "PC2"])
plot = df_pca.hvplot.scatter(x="PC1", y="PC2")
hvplot.save(plot, '../resources/pca3.html')
```

![img](../resources/pca3.png)


<a id="visualize-elbow-curve"></a>

## Visualize Elbow Curve

We will fit the PCA data to the KMeans model now, instead of the complete set of dimensions from before.

```python
file = "../resources/elbow2.png"
sse = {}
K = range(1,10)
for k in K:
    kmeanmodel = KMeans(n_clusters=k).fit(X_pca)
    sse[k]= kmeanmodel.inertia_

# Plot
fig, ax = plt.subplots(figsize=(6, 4))
ax.plot(list(sse.keys()), list(sse.values()))
ax.set_xlabel('k')
ax.set_ylabel('SSE')
ax.set_title('Elbow Method')
fig.savefig(file)
print(file)
```

<div class="org" id="org20056bf">

<div id="org3dbfd72" class="figure">
<p><img src="../resources/elbow2.png" alt="elbow2.png" />
</p>
</div>

</div>


<a id="select-amount-of-clusters-and-build-model"></a>

## Select Amount of Clusters and build Model

```python
model = KMeans(n_clusters=4, random_state=42).fit(X_pca)
y_pred = model.fit_predict(X_pca)
print(y_pred[:10])
```

    [1 1 1 1 1 1 1 1 1 1]


<a id="visualize-the-results"></a>

## Visualize the Results

```python
df_pca['cluster'] = y_pred

plot = df_pca.hvplot.scatter(x="PC1", y="PC2", by="cluster")
hvplot.save(plot, '../resources/pca4.html')
```

![img](../resources/pca4.png)

We can observe that we have two cuts in our two dimensions, a negative correlation between PCA1 and PCA2. Then in color we have our clusters very defined.

```python
df_y = pd.DataFrame(y_pred, columns=['Cluster'])
combined = df.join(df_y, how='inner')
print(combined.head())
```

       Age  M/F     5K     10K     15K     20K    Half     25K     30K     35K     40K   Pace  Official Time  Age Group  Cluster
    0   30    1  911.0  1821.0  2748.0  3676.0  3869.0  4583.0  5521.0  6436.0  7305.0  293.0         7677.0          0        1
    1   29    1  910.0  1822.0  2746.0  3676.0  3870.0  4584.0  5521.0  6436.0  7306.0  293.0         7679.0          0        1
    2   34    1  914.0  1822.0  2747.0  3677.0  3871.0  4584.0  5521.0  6436.0  7305.0  294.0         7687.0          1        1
    3   32    1  914.0  1824.0  2747.0  3676.0  3871.0  4584.0  5521.0  6436.0  7328.0  295.0         7734.0          0        1
    4   26    1  912.0  1821.0  2746.0  3675.0  3868.0  4583.0  5521.0  6436.0  7317.0  296.0         7735.0          0        1

```python
file = "../resources/boxplot3.png"

combined.boxplot(['Pace'], by=['M/F', 'Cluster'])

plt.savefig(file)
print(file)
```

<div class="org" id="org426ded2">

<div id="orgc3dc229" class="figure">
<p><img src="../resources/boxplot3.png" alt="boxplot3.png" />
</p>
</div>

</div>

There seems to be a pattern. The pattern is the same as the cut we saw in the scatter plot. So clusters 1, 3 seem to have a significantly higher pace, while clusters 0, 2 have a significantly lower pace.


<a id="display-summary-statistics"></a>

## Display summary statistics

We can take a look at the summary statistics to try to find a way to suggest a cut for the basis so we can create categories for the marathon.

```python
print(combined.groupby(['M/F', 'Cluster']).describe()['Pace'])
```

                  count        mean        std    min    25%    50%    75%     max
    M/F Cluster
    0   0        5695.0  598.411414  88.711040  402.0  532.0  578.0  648.0  1021.0
        3        6286.0  528.745625  85.410255  329.0  473.0  506.0  567.0   863.0
    1   1        8289.0  463.627337  81.862134  293.0  409.0  443.0  497.0   848.0
        2        6377.0  572.265799  99.287868  349.0  494.0  551.0  633.0  1062.0

Setting a value of 525 would be a good place to split the first quartile of the fast to the thrid quartile of the slowest. So we find where of the two categories come close and then declare a line where we make a decision to separate them.

**Note** that we don&rsquo;t actively try to understand the meaning of PCA dimensions, we make a decision at the end of the analysis, after we already made the prediction (clustering) with our model.


<a id="hierarchical-clustering"></a>

# Hierarchical Clustering

Hierarchical Clustering builds a hierarchy of connections based on distance of the clusters. The dendogram allows us to read this hierarchy.


<a id="dendograms"></a>

## Dendograms

Dendograms are tree-like structures where each cluster starts at the bottom.

![img](../resources/dendogram.png)

Each dendrite ending at the bottom is a cluster. For each cluster, the algorithm will find the closest neighbor and it will put it beside it, then connects them with a dendrite that goes as high as the distance between the clusters. So the lower the connection is positioned in the dendogram, the closer they are in the distribution. This replicates with the subsequent connections in the dendogram.

For this hierarchy to be formed, we need to have the data normalized. We will use the `dendogram` from `scipy` to create these hierachies.

There are different types of linkage methods.

1.  Single: The difference between two clusters is defined by the closest distance between two clusters.
2.  Complete: The difference between two clusters is defined by the farthest distance between two cluters.
3.  Ward: This method is based on the squared euclidean distance between clusters. It&rsquo;s the method often used as default.


<a id="hierarchical-cluster-example"></a>

# Hierarchical Cluster Example


<a id="imports"></a>

## Imports

```python
from sklearn.preprocessing import normalize
from scipy.cluster.hierarchy import dendrogram, linkage
from sklearn.cluster import AgglomerativeClustering
import matplotlib.pyplot as plt
from pathlib import Path
import pandas as pd
import numpy as np
```


<a id="load-and-normalize-the-data"></a>

## Load and Normalize the Data

```python
file = Path('../resources/wholesale_customers.csv')
df = pd.read_csv(file)
print(df.sample(5))
```

         Channel  Region  Fresh   Milk  Grocery  Frozen  Detergents_Paper  Delicassen
    253        1       1  29526   7961    16966     432               363        1391
    68         1       3   2446   7260     3993    5870               788        3095
    276        1       3  27901   3749     6964    4479               603        2503
    359        1       3    796   5878     2109     340               232         776
    46         2       3   3103  14069    21955    1668              6792        1452

```python
normalized = normalize(df)
print(normalized[:5])
```

    [[1.11821406e-04 1.67732109e-04 7.08332695e-01 5.39873747e-01
      4.22740825e-01 1.19648904e-02 1.49505220e-01 7.48085205e-02]
     [1.25321880e-04 1.87982820e-04 4.42198253e-01 6.14703821e-01
      5.99539873e-01 1.10408576e-01 2.06342475e-01 1.11285829e-01]
     [1.24839188e-04 1.87258782e-04 3.96551681e-01 5.49791784e-01
      4.79632161e-01 1.50119124e-01 2.19467293e-01 4.89619296e-01]
     [6.45937822e-05 1.93781347e-04 8.56836521e-01 7.72541635e-02
      2.72650355e-01 4.13658581e-01 3.27490476e-02 1.15493683e-01]
     [7.91877886e-05 1.18781683e-04 8.95415919e-01 2.14202968e-01
      2.84996851e-01 1.55010096e-01 7.03583502e-02 2.05294342e-01]]


<a id="create-the-dendogram"></a>

## Create the Dendogram

We will create our mergings first, then use the `dendrogram` function from `scipy` to create the dendogram.

```python
file = "../resources/dendrogram1.png"

mergings = linkage(normalized, method='ward')
plt.figure(figsize=(10, 4))

dendrogram(
    mergings,
    leaf_rotation=90,
    leaf_font_size=5
)
plt.savefig(file)

print(file)
```

<div class="org" id="org0c7fd7b">

<div id="org64bf5a5" class="figure">
<p><img src="../resources/dendrogram1.png" alt="dendrogram1.png" />
</p>
</div>

</div>

**Note** that we have way too many cases which results in way too many branches. So we must map everything into 4 clusters to get a better visualization result.


<a id="remap-observations-into-clusters"></a>

## Remap Observations into Clusters

```python
df2 = pd.DataFrame(normalized)
df2.columns = df.columns

model = AgglomerativeClustering(
    n_clusters=4,
    affinity='euclidean',
    linkage='ward'
)
labels = model.fit_predict(df)
print(labels[:10])
```

    [2 2 2 2 3 2 2 2 2 0]


<a id="run-the-dendogram-again"></a>

## Run the Dendogram Again

We will use the `labels` argument of the dendrogram function and pass the `labels` as a numpy array.

```python
file = "../resources/dendrogram2.png"

mergings = linkage(normalized, method='ward')

plt.figure(figsize=(10, 4))
dendrogram(
    mergings,
    labels=np.array(labels),
    leaf_rotation=90,
    leaf_font_size=5
)
plt.savefig(file)

print(file)
```

<div class="org" id="org85cd04c">

<div id="org9c8cd30" class="figure">
<p><img src="../resources/dendrogram2.png" alt="dendrogram2.png" />
</p>
</div>

</div>

We can see a few groups from the dendrogram but we can also try to visualize it in a 2d plane as we&rsquo;ve before, then find similarities between them.

```python
file = "../resources/dendro_clusters2d.png"

plt.figure(figsize=(5, 4))

plt.scatter(df2['Grocery'], df2['Fresh'], c=labels)

plt.savefig(file)
print(file)
```

<div class="org" id="orga0b1f0c">

<div id="org5cf69ab" class="figure">
<p><img src="../resources/dendro_clusters2d.png" alt="dendro_clusters2d.png" />
</p>
</div>

</div>
